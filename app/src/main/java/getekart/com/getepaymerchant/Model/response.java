package getekart.com.getepaymerchant.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class response implements Serializable {

    @SerializedName("username")
    private String username;

    @SerializedName("permission")
    private Lpermission permission;


    @SerializedName("status")
    private String status;


    @SerializedName("message")
    private String message;

    public response(String username, Lpermission lpermission, String status, String message) {
        this.username = username;
        this.permission = lpermission;
        this.status = status;
        this.message = message;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Lpermission getPermission() {
        return permission;
    }

    public void setPermission(Lpermission permission) {
        this.permission = permission;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
