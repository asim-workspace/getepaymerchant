package getekart.com.getepaymerchant.Model;

public class post
{
    private String username;
    private String password;
    private String mid;
    private String signature;

    public post(String username, String password, String mid, String signature) {
        this.username = username;
        this.password = password;
        this.mid = mid;
        this.signature = signature;
    }

    public post()
    {

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    @Override
    public String toString() {
        return "post{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", mid='" + mid + '\'' +
                ", signature='" + signature + '\'' +
                '}';
    }
}
