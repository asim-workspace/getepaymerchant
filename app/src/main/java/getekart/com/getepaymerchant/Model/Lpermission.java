package getekart.com.getepaymerchant.Model;

import android.support.v7.app.AppCompatActivity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Lpermission implements Serializable{


    private String id;
    private String pname;

    public Lpermission(String id, String pname) {
        this.id = id;
        this.pname = pname;
    }

    public Lpermission()
    {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    @Override
    public String   toString() {
        return "Lpermission{" +
                "id='" + id + '\'' +
                ", pname='" + pname + '\'' +
                '}';
    }
}
