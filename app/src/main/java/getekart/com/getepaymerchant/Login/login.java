package getekart.com.getepaymerchant.Login;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import getekart.com.getepaymerchant.Model.post;
import getekart.com.getepaymerchant.Model.response;
import getekart.com.getepaymerchant.R;
import getekart.com.getepaymerchant.main.Home;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class login extends AppCompatActivity {
Button b1;
EditText e1,e2;

String username,password,mid,signature;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        String hashtext;

        username="Test_Merchant";
        password="Test@123";
        mid="6irdwDznArIvUbS3POLsqg==";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(password.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            hashtext = number.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }

        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }

        e1=findViewById(R.id.usernametext);
        e2=findViewById(R.id.passtext);


        b1 = findViewById(R.id.login);
        final String finalHashtext = hashtext;
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



        String[] str = {mid,password,username};
        final String SIGNATURE = signatureGeneration(finalHashtext, str);
        signature=SIGNATURE;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://35.200.153.165:8080/getepay/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RetrofitInterface service = retrofit.create(RetrofitInterface.class);

        Call<response> call = service.getStringScalar(new post("test_Merchant","Test@123","6irdwDznArIvUbS3POLsqg==","7d4cb6469a8a40d0f505b283de54394b08080956ccff9be53b89f4ec2c162afd7a4385c3394fecd391ccdaecd7a974f47f00eb46a43a7f393305dc3ca82e0be9"));
        call.enqueue(new Callback<response>() {
            @Override
            public void onResponse(Call<response> call, Response<response> response) {
                // response.body() have your LoginResult fields and methods  (example you have to access error then try like this response.body().getError() )
                // response.body() have your LoginResult fields and methods  (example you have to access error then try like this response.body().getError() )
                String username1 = response.body().getUsername();
//                String id = responses.body().getLpermission().getId();
                String status = response.body().getStatus();
                String message = response.body().getMessage();
                if(status.equals("true"))
                {
                    Intent intent=new Intent(login.this,Home.class);
                    startActivity(intent);
                    Toast.makeText(login.this, "Status true", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(login.this, "Status false", Toast.LENGTH_SHORT).show();
                }
//                Intent intent = new Intent();
//                intent.putExtra("response", response.body());
//                setResult(Activity.RESULT_OK, intent);
//                finish();


            }

            @Override
            public void onFailure(Call<response> call, Throwable t) {
                //for getting error in network put here Toast, so get the error on network
                System.out.println("result negetive=>" + t.getMessage());
                t.printStackTrace();

            }

//            @Override
//            public void onResponse(Call<response> call, Response<PaymentResult> response) {
//
//            }
//
//            @Override
//            public void onFailure(Call<PaymentResult> call, Throwable t) {
//            }
        });
    }
});

    }
//------------------------------------------SIGNATURE GENRATION-----------------------------------------------------------------
    public static String signatureGeneration(String password, String[] param) {
        return getEncodedValueWithSha2(password, param);
    }

    public static String getEncodedValueWithSha2(String hashKey, String... param) {
        String resp = null;

        StringBuilder sb = new StringBuilder();
        for (String s : param) {
            sb.append(s);
        }

        try {
            System.out.println("[getEncodedValueWithSha2]String to Encode =" + sb.toString());
            resp = byteToHexString(encodeWithHMACSHA2(sb.toString(), hashKey));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    /*
     * Hashing using key with HMACSHA512
     */
    public static byte[] encodeWithHMACSHA2(String text, String keyString)
            throws java.security.NoSuchAlgorithmException, java.security.InvalidKeyException,
            java.io.UnsupportedEncodingException {

        java.security.Key sk = new javax.crypto.spec.SecretKeySpec(keyString.getBytes("UTF-8"), "HMACSHA512");
        javax.crypto.Mac mac = javax.crypto.Mac.getInstance(sk.getAlgorithm());
        mac.init(sk);

        byte[] hmac = mac.doFinal(text.getBytes("UTF-8"));

        return hmac;
    }

    /*
     * Convert from byte array to HexString
     */
    public static String byteToHexString(byte byData[]) {
        StringBuilder sb = new StringBuilder(byData.length * 2);

        for (int i = 0; i < byData.length; i++) {
            int v = byData[i] & 0xff;
            if (v < 16)
                sb.append('0');
            sb.append(Integer.toHexString(v));
        }

        return sb.toString();
    }
//----------------------------------------------SIGNATURE GENRATION END------------------------------------------------------
}
