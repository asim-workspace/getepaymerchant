package getekart.com.getepaymerchant.Login;
import getekart.com.getepaymerchant.Model.post;
import getekart.com.getepaymerchant.Model.response;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface RetrofitInterface {
    @Headers("Content-Type: application/json"  )
     @POST("merchantlogin/appMerchantLogin")
     Call<response> getStringScalar(@Body post body);
}
